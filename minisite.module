<?php

/**
 * It return theme according purl
 * @return [type] [description]
 */
function minisite_custom_theme() {
  $mytheme = minisite_get_og_purl_theme();
  if (isset($mytheme->group_theme_value)) {
      return $mytheme->group_theme_value;
  }
}

function minisite_get_og_purl_theme() {
  $path = $_SERVER['REQUEST_URI'];
  $purls = minisite_og_purl_themes();

  foreach ($purls as $purl) {
      if(preg_match('/(' . $purl->value . ')/', $path, $matches)) {
      return $purl;
    }
  }
}

/**
 * It links two tables purl and og theme to return a consolidated array
 * @return [type] [description]
 */
function minisite_og_purl_themes() {
  $query = db_select('purl', 'p');
  $query->join('field_data_group_theme', 'f', 'p.id = f.entity_id');
  $query->fields('f', array('entity_id', 'group_theme_value'));
  $query->fields('p', array('value'));
  $result = $query->execute()->fetchAllAssoc('entity_id');
  return $result;
}

/**
 *  Implementation of hook_node_update
 * @param  [type]
 * @return [type]
 */
function minisite_node_update($node) {
  if (in_array($node->type, array('minisite'))) {

    // update theme of all nodes belonging to a group
    minisite_update_theme($node->nid);

    $menu_name = 'menu-og-' . $node->nid;

    if (empty(_minisite_menu_exists($menu_name))) {
      // save menu home
      $item_home = array (
        'link_title' => 'Inicio',
        'link_path' => 'node/' . $node->nid,
        'menu_name' => $menu_name,
        'weight' => 0,
        'plid' => 0,
      );
      menu_link_save($item_home);

      // save menu noticias
      $item_noticias = array (
        'link_title' => 'Noticias',
        'link_path' => 'noticias/' . $node->nid,
        'menu_name' => $menu_name,
        'weight' => 0,
        'plid' => 0,
      );
      menu_link_save($item_noticias);

      // save menu publicaciones
      $item_publicaciones = array (
        'link_title' => 'Publicaciones',
        'link_path' => 'publicaciones/' . $node->nid,
        'menu_name' => $menu_name,
        'weight' => 0,
        'plid' => 0,
      );
      menu_link_save($item_publicaciones);

    }

  }
}

/**
 * Check out if menu exists
 * @param  [type] $menu_name [description]
 * @return [type]            [description]
 */
function _minisite_menu_exists($menu_name) {
  $query = db_select('menu_links', 't');
  $query->fields('t', array('menu_name', 'mlid', 'link_path', 'router_path', 'link_title'));
  $query->condition('t.menu_name', $menu_name);
  $query->condition('t.link_title', array('Noticias', 'Publicaciones'), 'IN');
  return $query->execute()->fetchAllAssoc('mlid');
}

/**
 * Update themes
 * @param  [type]
 * @return [type]
 */
function minisite_update_theme($gid) {

  $query = db_select('field_data_group_theme', 'f');
  $query->fields('f', array('group_theme_value'));
  $query->condition('entity_id', $gid);
  $query->condition('bundle', 'minisite');
  $theme = $query->execute()->fetchField();

  $children = minisite_og_get_children($gid);

  foreach ($children as $child) {
    db_update('field_data_group_theme')
      ->fields(array('group_theme_value' => $theme))
      ->condition('entity_id', $child)
      ->condition('bundle', 'minipage')
      ->execute();
  }
  drupal_flush_all_caches();
}

/**
 * get all children of a group
 * @param  [type]
 * @return [type]
 */
function minisite_og_get_children($gid){
  $query = db_select('og_membership', 'ogm');
  $query->fields('ogm', array('etid'));
  $query->condition('gid', $gid);
  $query->condition('entity_type', 'node');
  $result = $query->execute()->fetchCol(0);
  // array_push($result, $gid);
  return $result;
}


/**
 * @file
 * Replaces all system object management pages in Drupal core with real views.
 */

/**
 * Implements hook_views_api().
 */
function minisite_views_api() {
  return array(
    'api' => 3,
  );
}

/**
 * hook_node_delete
 * Remove all children of a group
 * @param  [type] $node [description]
 * @return [type]       [description]
 */
function minisite_node_delete($node) {
  if ($node->type == 'minisite') {
    $children = minisite_og_get_children($node->nid);
    foreach ($children as $child) {
      node_delete($child);
    }
    drupal_set_message('El minisite fue borrado junto con todos sus componentes', 'status');
  }
  //drupal_goto('admin/content');
}

/**
 * hook_node_insert
 * @param  [type] $node [description]
 * @return [type]       [description]
 */
function minisite_node_insert($node) {
  if (in_array($node->type, array('minisite'))) {
    drupal_set_message('Por favor actualizar el contenido de nuevo para crear menus', 'status');
  }
}


/**
 * Add a menu item that should appear in the group admin page.
 */
function minisite_og_ui_get_group_admin() {

  $items = array();

  $purl_theme = minisite_get_og_purl_theme();

  $parameters = array('og_group_ref' => $gnode->nid, 'destination' => 'node/' . $gnode->nid . '/group');

  if (isset($purl_theme) && user_access('administer nodes')) {
    $gnode = node_load($purl_theme->entity_id);
    $items['add_minipage'] = array(
      'title' => t('Add page'),
      'description' => t('Add page.'),
      //'href' => 'node/add/minipage?og_group_ref=' . $gnode->nid . '&destination=node/' . $gnode->nid . '/group',
      'href' => 'node/add/minipage',
      'localized_options' => array('query' => array('og_group_ref' => $gnode->nid, 'destination' => 'node/' . $gnode->nid . '/group'))
    );
  }
  return $items;
}

/**
 * Implementation of hook_form_node_delete_confirm_alter
 * @param  [type] &$form       [description]
 * @param  [type] &$form_state [description]
 * @param  [type] $form_id     [description]
 * @return [type]              [description]
 */
function minisite_form_node_delete_confirm_alter(&$form, &$form_state, $form_id) {
  $form['#submit'][] = 'minisite_node_delete_confirm_submit';
}

function minisite_node_delete_confirm_submit($form, &$form_state) {
  //@TODO: Post-node-delete code here
  if ($form['#node']->type == 'minisite') {
    drupal_goto('<front>');
  }
}

